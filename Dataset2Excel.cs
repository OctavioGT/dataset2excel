﻿
namespace Xsoft
{
    using System;
    using ClosedXML.Excel;
    using System.Data;
    using System.IO;
    using System.Text;
     
    /// <summary>
    ///  Herramientas    
    /// </summary>
    public static class Herramientas
    {
        /// <summary>
        /// Objeto de estilo para la tabla de excel
        /// </summary>        
       
        public  class  Dataset2ExcelEstilo
        {               
            /// <summary>
            /// Color de fondo del título (hexadecimal)
            /// </summary>               
            public string TituloColorDeFondo { get; set; }
            /// <summary>
            /// Color de fondo la letra del título (hexadecimal)
            /// </summary>               
            public string TituloColorDeLetra { get; set; }
            /// <summary>
            /// Color de fondo de las columnas (hexadecimal)
            /// </summary>
               
            public string ColumnaColorDeFondo { get; set; }

            /// <summary>
            /// Color de fondo de la letra de las columnas (hexadecimal)
            /// </summary>               
            public string ColumnaColorDeLetra { get; set; }
        }



        public static MemoryStream Dataset2Excel(DataSet ds, string[] Titulos = null, Dataset2ExcelEstilo Estilo = null, bool AjustarColumnas = true)
        {
            XLWorkbook workbook = new XLWorkbook();

            int nTables = ds.Tables.Count;

            // Tables
            for (int i = 0; i < nTables; i++)
            {
                DataTable dt = ds.Tables[i];

                var worksheet = workbook.Worksheets.Add(dt.TableName);

                int nFields = dt.Columns.Count;
                int nRows = dt.Rows.Count;

                // Rows
                for (int j = 0; j < nRows; j++)
                {
                    // Fields
                    for (int k = 0; k < nFields; k++)
                    {
                        if (j == 0)
                        {                                                                                          
                            worksheet.Cell(1, k + 1).Style.Font.Bold = true;
                            worksheet.Cell(1, k + 1).Value = dt.Columns[k].ColumnName;
                        }

                        worksheet.Cell(j + 2, k + 1).SetDataType(XLCellValues.Text);
                        worksheet.Cell(j + 2, k + 1).Value = dt.Rows[j][k];

                        if (AjustarColumnas)
                        {
                            worksheet.Columns().AdjustToContents();
                        }                        
                    }
                }

                //Titulo
                Dataset2ExcelEstilo E = new Dataset2ExcelEstilo();

                // Valores default
                E.TituloColorDeFondo = "#1D61AA";
                E.TituloColorDeLetra = "#FFFFFF";
                E.ColumnaColorDeFondo = "#F2F2F2";
                E.ColumnaColorDeLetra = "#000000";

                if (Estilo != null)
                {
                    if (Estilo.TituloColorDeFondo != null) {E.TituloColorDeFondo = Estilo.TituloColorDeFondo;}
                    if (Estilo.TituloColorDeLetra != null) {E.TituloColorDeLetra = Estilo.TituloColorDeLetra;}
                    if (Estilo.ColumnaColorDeFondo != null) {E.ColumnaColorDeFondo = Estilo.ColumnaColorDeFondo;}
                    if (Estilo.ColumnaColorDeLetra != null) {E.ColumnaColorDeLetra = Estilo.ColumnaColorDeLetra;}
                }                   

                if (Titulos != null)
                {
                    string Titulo = "";
                    try
                    {
                        Titulo = Titulos[i];
                    }
                    catch (Exception)
                    {
                        Titulo = "";
                    }

                       
                    // Agregar fila a la hoja y su valor
                    worksheet.Row(1).InsertRowsAbove(1);
                    worksheet.Cell(1, 1).Value = Titulo;
                    worksheet.Range(1, 1, 1, nFields).Merge();

                    // Estilo Titulo
                    worksheet.Cell(1, 1).Style.Font.Bold = true;
                    worksheet.Cell(1, 1).Style.Fill.BackgroundColor = XLColor.FromHtml(E.TituloColorDeFondo);
                    worksheet.Cell(1, 1).Style.Font.FontColor = XLColor.FromHtml(E.TituloColorDeLetra);
                    worksheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    // Estilo nombre de columnas
                    worksheet.Range(2, 1, 2, nFields).Style.Font.Bold = true;
                    worksheet.Range(2, 1, 2, nFields).Style.Fill.BackgroundColor = XLColor.FromHtml(E.ColumnaColorDeFondo);
                    worksheet.Range(2, 1, 2, nFields).Style.Font.FontColor = XLColor.FromHtml(E.ColumnaColorDeLetra);
                    worksheet.Range(2, 1, 2, nFields).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                }
                else {
                    worksheet.Range(1, 1, 1, nFields).Style.Font.Bold = true;
                    if (Estilo != null) { 
                        // Estilo nombre de columnas                           
                        worksheet.Range(1, 1, 1, nFields).Style.Fill.BackgroundColor = XLColor.FromHtml(E.ColumnaColorDeFondo);
                        worksheet.Range(1, 1, 1, nFields).Style.Font.FontColor = XLColor.FromHtml(E.ColumnaColorDeLetra);
                        worksheet.Range(1, 1, 1, nFields).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    }
                        
                }                                     
            }

            MemoryStream fs = new MemoryStream();
            workbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        public static bool Dataset2Excel(string NombreDeArchivo, DataSet ds, string[] Titulos = null, Dataset2ExcelEstilo Estilo = null, bool AjustarColumnas = true)
        {
            bool r = false;
            try
            {
           
            XLWorkbook workbook = new XLWorkbook();

            int nTables = ds.Tables.Count;

            // Tables
            for (int i = 0; i < nTables; i++)
            {
                DataTable dt = ds.Tables[i];

                var worksheet = workbook.Worksheets.Add(dt.TableName);

                int nFields = dt.Columns.Count;
                int nRows = dt.Rows.Count;

                // Rows
                for (int j = 0; j < nRows; j++)
                {
                    // Fields
                    for (int k = 0; k < nFields; k++)
                    {
                        if (j == 0)
                        {
                            worksheet.Cell(1, k + 1).Style.Font.Bold = true;
                            worksheet.Cell(1, k + 1).Value = dt.Columns[k].ColumnName;
                        }

                        worksheet.Cell(j + 2, k + 1).SetDataType(XLCellValues.Text);
                        worksheet.Cell(j + 2, k + 1).Value = dt.Rows[j][k];

                        if (AjustarColumnas==true)
                        {
                            worksheet.Columns().AdjustToContents();
                        }
                       
                    }
                }

                //Titulo
                Dataset2ExcelEstilo E = new Dataset2ExcelEstilo();

                // Valores default
                E.TituloColorDeFondo = "#1D61AA";
                E.TituloColorDeLetra = "#FFFFFF";
                E.ColumnaColorDeFondo = "#F2F2F2";
                E.ColumnaColorDeLetra = "#000000";

                if (Estilo != null)
                {
                    if (Estilo.TituloColorDeFondo != null) { E.TituloColorDeFondo = Estilo.TituloColorDeFondo; }
                    if (Estilo.TituloColorDeLetra != null) { E.TituloColorDeLetra = Estilo.TituloColorDeLetra; }
                    if (Estilo.ColumnaColorDeFondo != null) { E.ColumnaColorDeFondo = Estilo.ColumnaColorDeFondo; }
                    if (Estilo.ColumnaColorDeLetra != null) { E.ColumnaColorDeLetra = Estilo.ColumnaColorDeLetra; }
                }

                if (Titulos != null)
                {
                    string Titulo = "";
                    try
                    {
                        Titulo = Titulos[i];
                    }
                    catch (Exception)
                    {
                        Titulo = "";
                    }


                    // Agregar fila a la hoja y su valor
                    worksheet.Row(1).InsertRowsAbove(1);
                    worksheet.Cell(1, 1).Value = Titulo;
                    worksheet.Range(1, 1, 1, nFields).Merge();

                    // Estilo Titulo
                    worksheet.Cell(1, 1).Style.Font.Bold = true;
                    worksheet.Cell(1, 1).Style.Fill.BackgroundColor = XLColor.FromHtml(E.TituloColorDeFondo);
                    worksheet.Cell(1, 1).Style.Font.FontColor = XLColor.FromHtml(E.TituloColorDeLetra);
                    worksheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    // Estilo nombre de columnas
                    worksheet.Range(2, 1, 2, nFields).Style.Font.Bold = true;
                    worksheet.Range(2, 1, 2, nFields).Style.Fill.BackgroundColor = XLColor.FromHtml(E.ColumnaColorDeFondo);
                    worksheet.Range(2, 1, 2, nFields).Style.Font.FontColor = XLColor.FromHtml(E.ColumnaColorDeLetra);
                    worksheet.Range(2, 1, 2, nFields).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                }
                else
                {
                    worksheet.Range(1, 1, 1, nFields).Style.Font.Bold = true;
                    if (Estilo != null)
                    {
                        // Estilo nombre de columnas                           
                        worksheet.Range(1, 1, 1, nFields).Style.Fill.BackgroundColor = XLColor.FromHtml(E.ColumnaColorDeFondo);
                        worksheet.Range(1, 1, 1, nFields).Style.Font.FontColor = XLColor.FromHtml(E.ColumnaColorDeLetra);
                        worksheet.Range(1, 1, 1, nFields).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    }

                }
            }

            
            // Grabar archivo
            workbook.SaveAs(NombreDeArchivo);

            // Liberar Objeto
            workbook.Dispose();

            workbook = null;


            r = true;
            }
            catch (Exception)
            {
                r = false;
            }
            return r;
        }
                    
    }   
}
