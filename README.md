# Dataset2Excel #


### Summary ###
Genera un archivo de Excel a partir de un DataSet

### Ejemplo para descargar el archivo al cliente ###
##### VB.NET #####


    Dim dts As DataSet = getData()
    Dim stream As MemoryStream = Xsoft.Herramientas.Dataset2Excel(dts, {"Título del Reporte"}, New Xsoft.Herramientas.Dataset2ExcelEstilo())

    Response.Clear()
    Response.Buffer = True
    Response.AddHeader("content-disposition", "attachment; filename=" & "archivo.xlsx")
    Response.ContentType = "application/vnd.ms-excel"
    Response.BinaryWrite(stream.ToArray())
    Response.End() 

#### C# ###
	DataSet dts = getData();      
    MemoryStream stream = Xsoft.Herramientas.Dataset2Excel(dts,  new string[] {"Título del Reporte"}, new Xsoft.Herramientas.Dataset2ExcelEstilo());

    Response.Clear();
    Response.Buffer = true;
    Response.AddHeader("content-disposition", "attachment; filename=" + "archivo.xlsx");
    Response.ContentType = "application/vnd.ms-excel";
    Response.BinaryWrite(stream.ToArray());
    Response.End();

### Ejemplo para generar el archivo en el servidor ###
##### VB.NET #####
	Dim dts As DataSet = getData()
    Dim RutayArchivo As String = Server.MapPath("/") & "Archivo.xlsx"
    Dim r As Boolean = Xsoft.Herramientas.Dataset2Excel(RutayArchivo, dts, {"Titulo del REPORTE"}, New Xsoft.Herramientas.Dataset2ExcelEstilo())
    If r Then
    	Response.Write("<a href='Archivo.xlsx' target='_blank' >Descargar</a>")
    End If

#### C# ###
	DataSet dts = getData();
    string RutayArchivo = Server.MapPath("/") + "Archivo.xlsx";
    bool r = Xsoft.Herramientas.Dataset2Excel(RutayArchivo, dts, new string[] { "Titulo del REPORTE" }, new Xsoft.Herramientas.Dataset2ExcelEstilo());
    if (r) {
        Response.Write("<a href='Archivo.xlsx' target='_blank' >Descargar</a>");
    }

### Opciones de estilo para la hoja de Excel ###
Los valores son en hexadecimal

* Título
	* Color de fondo (default:#1D61AA)
	* Color de letra (default:#FFFFFF)
* Nombre de columnas
	* Color de fondo (default:#F2F2F2)
	* Color de letra (default:#000000)


### Notas ###
* Los ejemplos están escritos para ASP.NET, pero funciona en winforms.

### Dependecias ###
* ClosedXML 

### Nuget ###
 * Install-Package Dataset2Excel.dll
 * https://www.nuget.org/packages/Dataset2Excel.dll/

### Contact ###
lacho.gutierrez@gmail.com


P.D. Let's go play !!!